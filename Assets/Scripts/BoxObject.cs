﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxObject: MonoBehaviour {

    public Transform spawn;

    private void Start() {
        Box = this;
        Reset();
    }
    void Update() {
        transform.Rotate(new Vector3(0, -45, 0) * Time.deltaTime);
    }

    public static BoxObject Box { get; set; }
    public bool IsTaken { get; set; }

    public void Take(Transform newTransform) {
        IsTaken = true;
        transform.parent = newTransform;
        transform.localPosition = new Vector3(0, 0, 2);
        PlatformObject.Platform.HasItem = false;
    }

    public void Reset() {
        transform.parent = spawn;
        IsTaken = false;
        transform.localPosition = new Vector3(0, 4, 0);

        if(PlatformObject.Platform != null) PlatformObject.Platform.HasItem = true;
    }
}
