﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

	public Button valleySceneButton;
	public Button townSceneButton;
	public Button exitButton;

	public string valleySceneName;
	public string townSceneName;

	public void ValleySceneButtonPressed() {
		SceneManager.LoadScene (valleySceneName, LoadSceneMode.Single);
	}

	public void TownSceneButtonPressed() {
		SceneManager.LoadScene (townSceneName, LoadSceneMode.Single);
	}

	public void ExitButtonPressed() {
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}
}