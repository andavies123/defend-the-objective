﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MoveableObject : MonoBehaviour {

	protected virtual void Start () {
        RigidBody = GetComponent<Rigidbody>();
	}
    protected virtual void Update() {
        CheckHealth();
        CheckIfGrounded();
        CheckIfDead();
    }
    protected virtual void FixedUpdate() {
        Move();
    }





    public Rigidbody RigidBody { get; private set; }
    public Vector3 MoveDirection;
    public Vector3 LookDirection;

    public float MaxHealth { get; set; }
    public float Health { get; set; }
    public float Speed;
    public float WalkSpeed { get; set; }
    public float RunSpeed { get; set; }
    public float JumpForce { get; set; }

    public bool IsAlive { get; set; }
    public bool IsGrounded;





    public void Move() { RigidBody.MovePosition(RigidBody.position + MoveDirection * Speed * Time.fixedDeltaTime); }
    public void Walk() { Speed = WalkSpeed; }
    public void Run() { Speed = RunSpeed; }
    public void Jump() { if (IsGrounded) RigidBody.AddForce(transform.up * JumpForce);}
    public void Heal(float health) { Health += health; }
    public void Hurt(float health) { Health -= health; }





    private void CheckHealth() {
        if (Health <= 0) {
            IsAlive = false;
            Health = 0;
        }
        else if (Health > MaxHealth) Health = MaxHealth;
    }
    private void CheckIfGrounded() {
        RaycastHit hit;
        Debug.DrawRay(transform.position, Vector3.down * (transform.localScale.y + 0.1f), Color.red);
        if (Physics.Raycast(transform.position, Vector3.down, out hit, transform.localScale.y + 0.1f)) {
            IsGrounded = hit.transform.gameObject.name.Contains("Terrain");
        }
        else IsGrounded = false;
    }
    private void CheckIfDead() { if (!IsAlive) OnDeath(); }





    protected abstract void OnDeath();
}
