﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserInterface : MonoBehaviour {

    public static UserInterface userInterface;

    public Text ammoText;
    public Text waveText;
    public Text hitmarkerText;
    public Text enemiesLeftText;
    public Text gameOverText;
    public Text pausedText;
    public Text healthText;
    public Button menuButton;
    public Button resumeButton;
    public Button restartButton;
    public Button gameOverMenuButton;
    public Image healthBar;

	public string currentSceneName;

    void Start() {
        userInterface = this;
        HitmarkerTimer = 0;
        IsGameOver = false;
        IsPaused = false;
    }

    void Update() {
        UpdateHealthBar();
        UpdateAmmoText();
        UpdateWaveText();
        UpdateEnemiesLeftText();
        UpdateHitmarkerText();
        UpdateGameOverText();
        UpdateIsPaused();

        if(Input.GetKeyDown(KeyCode.Escape)) {
            IsPaused = true;
        }
    }


    public float HitmarkerTimer { get; set; }
    public bool IsGameOver { get; set; }
    public bool IsPaused { get; set; }




    public void UpdateHealthBar() {
        healthBar.rectTransform.sizeDelta = new Vector2(PlayerObject.Player.Health/PlayerObject.Player.MaxHealth*200, healthBar.rectTransform.sizeDelta.y);
        healthText.text = PlayerObject.Player.Health + " / " + PlayerObject.Player.MaxHealth;
    }

    public void MenuButtonPress() {
        SceneManager.LoadScene("MenuScene", LoadSceneMode.Single);
    }

    public void ResumeButtonPress() {
        IsPaused = false;
    }

    public void RestartButtonPress() {
		SceneManager.LoadScene(currentSceneName, LoadSceneMode.Single);
    }

    public void UpdateAmmoText() {
		switch (PlayerObject.Player.CurrentWeapon.Type) {
		case GunObject.GunType.Pistol:
			ammoText.text = PlayerObject.Player.CurrentWeapon.BulletsInCurrentMagazine + " / " + PlayerObject.Player.Ammo.pistolAmmo;
			break;
		case GunObject.GunType.MachineGun:
			ammoText.text = PlayerObject.Player.CurrentWeapon.BulletsInCurrentMagazine + " / " + PlayerObject.Player.Ammo.machineGunAmmo;
			break;
		}
    }

    public void UpdateWaveText() {
        waveText.text = "Wave " + MapScript.map.CurrentWave;
    }

    public void UpdateEnemiesLeftText() {
        enemiesLeftText.text = "Enemies Left: " + MapScript.map.EnemiesLeft;
    }

    public void UpdateHitmarkerText() {
        if(HitmarkerTimer <= 0) {
            hitmarkerText.text = "";
        }
        else {
            hitmarkerText.text = "X";
        }
        HitmarkerTimer -= Time.deltaTime * 1000;
    }

    public void UpdateGameOverText() {
        if (IsGameOver) {
            gameOverText.text = "GAME OVER";
            Time.timeScale = 0;
            restartButton.gameObject.SetActive(true);
            gameOverMenuButton.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else {
            gameOverText.text = "";
            restartButton.gameObject.SetActive(false);
            gameOverMenuButton.gameObject.SetActive(false);
        }
    }

    public void UpdateIsPaused() {
        if(IsPaused && !IsGameOver) {
            pausedText.text = "PAUSED";
            Time.timeScale = 0;
            menuButton.gameObject.SetActive(true);
            resumeButton.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else if(!IsPaused && !IsGameOver){
            pausedText.text = "";
            Time.timeScale = 1;
            menuButton.gameObject.SetActive(false);
            resumeButton.gameObject.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}
