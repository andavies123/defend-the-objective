﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformObject : MonoBehaviour {

    public float hasItemRotateSpeed;
    public float noItemRotateSpeed;

    public bool baseObject;
    public bool ringObject;

    public GameObject outerRing;
    public GameObject middleRing;
    public GameObject innerRing;

    private void Start() {
        Platform = this;
        HasItem = true;
    }

    void Update() {
        if(baseObject) {
            if (HasItem) transform.Rotate(new Vector3(0, 45, 0) * Time.deltaTime * hasItemRotateSpeed);
            else transform.Rotate(new Vector3(0, 45, 0) * Time.deltaTime * noItemRotateSpeed);
        }
        else if(ringObject) {
            if(HasItem) {
                outerRing.GetComponent<RotateRing>().Rotate();
                middleRing.GetComponent<RotateRing>().Rotate();
                innerRing.GetComponent<RotateRing>().Rotate();
            }
            else {
                outerRing.GetComponent<RotateRing>().StopRotate();
                middleRing.GetComponent<RotateRing>().StopRotate();
                innerRing.GetComponent<RotateRing>().StopRotate();
            }
        }
    }

    public static PlatformObject Platform { get; set; }
    public bool HasItem { get; set; }
}

