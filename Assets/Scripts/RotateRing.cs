﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRing : MonoBehaviour {

    public float rotateXSpeed;
    public float rotateYSpeed;
    public float rotateZSpeed;

    public float RotateXSpeed { get; set; }
    public float RotateYSpeed { get; set; }
    public float RotateZSpeed { get; set; }

    void Start() { Rotate(); }


	void Update () {
        transform.Rotate(new Vector3(RotateXSpeed, RotateYSpeed, RotateZSpeed) * Time.deltaTime);
    }

    public void SetAllSpeeds(float x, float y, float z) {
        RotateXSpeed = x;
        RotateYSpeed = y;
        RotateZSpeed = z;
    }

    public void StopRotate() { SetAllSpeeds(0, 0, 0); }

    public void Rotate() { SetAllSpeeds(rotateXSpeed, rotateYSpeed, rotateZSpeed); }
}