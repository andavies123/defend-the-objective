﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammunition : MonoBehaviour
{

    public enum AmmoType { Pistol, MachineGun }

    public int pistolAmmo;
    public int machineGunAmmo;

    public int GetAmmo(AmmoType type, int ammoWanted)
    {
        if (ammoWanted <= 0) return 0;
        int returnAmmo = 0;
        switch (type)
        {
            case AmmoType.Pistol:
                if (pistolAmmo >= ammoWanted) returnAmmo = ammoWanted;
                else returnAmmo = pistolAmmo;
                pistolAmmo -= returnAmmo;
                break;
            case AmmoType.MachineGun:
                if (machineGunAmmo >= ammoWanted) returnAmmo = ammoWanted;
                else returnAmmo = machineGunAmmo;
                machineGunAmmo -= returnAmmo;
                break;
        }
        return returnAmmo;
    }

    public void AddAmmo(AmmoType type, int addedAmmo)
    {
        switch (type)
        {
            case AmmoType.Pistol: pistolAmmo += addedAmmo; break;
            case AmmoType.MachineGun: machineGunAmmo += addedAmmo; break;
        }
    }
}