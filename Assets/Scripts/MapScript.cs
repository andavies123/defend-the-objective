﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct EnemyTypes {
    public GameObject prefab;
    public int spawnChance;
}

public class MapScript : MonoBehaviour {

    public static MapScript map;

    public GameObject[] spawnPoints = new GameObject[8];
    public List<EnemyTypes> enemies;


    void Start () {
        map = this;

        CurrentWave = 0;
        EnemiesLeft = 0;
	}
	
	void Update () {
        if (EnemiesLeft <= 0) StartNextWave();
	}


    public int EnemiesPerWave { get; set; }
    public int EnemiesLeft { get; set; }
    public int CurrentWave { get; set; }

    private void StartNextWave() {
        CurrentWave++;
        EnemiesPerWave = EnemiesLeft = CurrentWave;
        StartCoroutine("SpawnEnemies");
    }

    private int CalculateTotalChance(List<EnemyTypes> enemyList) {
        int total = 0;
        for (int i = 0; i < enemyList.Count; i++) {
            total += enemyList[i].spawnChance;
        }
        return total;
    }

    private IEnumerator SpawnEnemies() {
        int totalChance = CalculateTotalChance(enemies);
        for(int index = 0; index < EnemiesPerWave; index++) {
            int randomChance = Random.Range(0, totalChance);
            int currentRandomMin = 0;
            int currentRandomMax = enemies[0].spawnChance;
            int enemyType = 0;
            for (int j = 0; j < enemies.Count; j++) {
                if (randomChance >= currentRandomMin && randomChance < currentRandomMax) {
                    enemyType = j;
                    break;
                }
                currentRandomMin = currentRandomMax;
                currentRandomMax += enemies[j + 1].spawnChance;
            }
            int RandomSpawnNumber = Random.Range(0, spawnPoints.Length);
            Instantiate(enemies[enemyType].prefab, spawnPoints[RandomSpawnNumber].transform.position, transform.rotation, spawnPoints[RandomSpawnNumber].transform);
            yield return new WaitForSeconds(1.5f);
        }
    }
}
