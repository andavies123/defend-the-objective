﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemyScript : EnemyScript {

    public int maxNumberDropsOnDeath;
    public float damage;
    public float timeBetweenAttacks;
    private bool canDealDamage;

	public int health;

	protected override void Start () {
        base.Start();
        MoveDirection = Vector3.zero;
		MaxHealth = Health = health;
        WalkSpeed = 5;
        RunSpeed = 7;
        Run();
        IsAlive = true;
        canDealDamage = true;
	}
	
	protected override void Update () {
        base.Update();
	}

    protected override void OnCollisionEnter(Collision collision) {}
    protected override void OnTriggerEnter(Collider other) {}

    void OnCollisionStay(Collision collision) {
        if (canDealDamage && collision.transform.name.Contains("Player")) {
            StartCoroutine(OnDamageDeal());
            collision.gameObject.GetComponent<PlayerObject>().Hurt(damage);
        }
    }

    protected override void OnDeath() {
        AudioSource.PlayClipAtPoint(deathSound, PlayerObject.Player.transform.position, 1f);
        int numRandomDrops = Random.Range(1, maxNumberDropsOnDeath+1);
        for (int i = 0; i < numRandomDrops; i++) {
            float randomSpacingX = Random.Range(0.0f, 1.0f);
            float randomSpacingZ = Random.Range(0.0f, 1.0f);
            int randomDrop = Random.Range(0, 2);
            switch (randomDrop)
            {
                case 0:
                    Instantiate(pistolAmmoPrefab, new Vector3(transform.position.x + randomSpacingX, transform.position.y, transform.position.z + randomSpacingZ), transform.rotation);
                    break;
                case 1:
                    Instantiate(machineGunAmmoPrefab, new Vector3(transform.position.x + randomSpacingX, transform.position.y, transform.position.z + randomSpacingZ), transform.rotation);
                    break;
            }
        }
        MapScript.map.EnemiesLeft--;
        Destroy(gameObject);
    }

    protected override void UpdateDirection() {
        transform.LookAt(GameObject.Find("Player").transform);
        MoveDirection = transform.forward;
    }

	protected override void UpdateBodyDamage() {

	}

    IEnumerator OnDamageDeal() {
        canDealDamage = false;
        yield return new WaitForSeconds(timeBetweenAttacks);
        canDealDamage = true;
    }
}
