﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MoveableObject {

    public bool StayStillForMenu;
    public GameObject pistolAmmoPrefab;
	public GameObject machineGunAmmoPrefab;

    public AudioClip deathSound;

    public GameObject[] bodyParts = new GameObject[4];

    protected override void Start () {
        base.Start();
        MoveDirection = Vector3.zero;
        MaxHealth = Health = 50;
        WalkSpeed = 5;
        RunSpeed = 2;
        Walk();
        IsAlive = true;

        HasItem = false;
	}
    protected override void Update () {
        base.Update();
        if(!StayStillForMenu) {
            UpdateDirection();
        }
        UpdateBodyDamage();
    }
    protected virtual void OnCollisionEnter(Collision collision) {
        if(collision.transform.parent != null && (collision.transform.parent.name.Contains("BaseSphere") || collision.transform.parent.name.Contains("Ring"))) {
            if(!BoxObject.Box.IsTaken) {
                BoxObject.Box.Take(transform);
                HasItem = true;
            }
        }
    }
    protected virtual void OnTriggerEnter(Collider other) {
        if (other.gameObject.name.Contains("Spawn Point") && HasItem) {
            UserInterface.userInterface.IsGameOver = true;
        }
    }

    protected override void OnDeath() {
        AudioSource.PlayClipAtPoint(deathSound, PlayerObject.Player.transform.position, 1f);
        if (HasItem) {
            BoxObject.Box.Reset();
        }
		int randomDrop = Random.Range (0, 2);
		switch (randomDrop) {
		case 0: 
			Instantiate (pistolAmmoPrefab, transform.position, transform.rotation);
			break;
		case 1:
			Instantiate (machineGunAmmoPrefab, transform.position, transform.rotation);
			break;
		}

        MapScript.map.EnemiesLeft--;
        Destroy(gameObject);
    }


    public bool HasItem { get; set; }


    protected virtual void UpdateDirection() {
        if(HasItem) {
            transform.LookAt(transform.parent);
        }
        else {
            transform.LookAt(GameObject.Find("Box Spawn").transform);
        }
        MoveDirection = transform.forward;
    }

    protected virtual void UpdateBodyDamage() {
        float percentDamageTaken = Health / MaxHealth;
		if (percentDamageTaken <= .8 && bodyParts[0].activeSelf) bodyParts[0].SetActive(false);
		if (percentDamageTaken <= .6 && bodyParts[1].activeSelf) bodyParts[1].SetActive(false);
		if (percentDamageTaken <= .4 && bodyParts[2].activeSelf) bodyParts[2].SetActive(false);
		if (percentDamageTaken <= .2 && bodyParts[3].activeSelf) bodyParts[3].SetActive(false);
    }
}
