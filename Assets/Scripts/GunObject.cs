﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunObject : MonoBehaviour 
{
    private static Vector3 hipFirePosition = new Vector3(0.5f, -0.25f, 0.75f);
    private static Vector3 adsPosition = new Vector3(0, -0.1f, 0.75f);

    public enum GunType { Pistol, MachineGun }
    public enum FireMode { Single, Auto }
    public enum Sounds { Shoot, Reload }

    private AudioSource audioData;

    protected virtual void Start() {
        audioData = GetComponent<AudioSource>();
        Spread = hipFireSpread;
    }
    protected virtual void Update() { 
        UpdateTimers();
        UpdateGunPosition();
    }



    public AudioClip ReloadSound;
    public AudioClip ShootSound;

    public int MaxBulletsPerMagazine;
    public int BulletsInCurrentMagazine { get; set; }

    public float DamagePerShot;
    public float TimeBetweenShots;
    public float TimeToReload;
    public float ShootTimer { get; set; }

    public float hipFireSpread;
    public float adsSpread = 0;
    public float Spread { get; set; }


    public bool IsReloading { get; set; }
    public bool IsAimedDownSight { get; set; }

    public GunType Type;
    public FireMode Mode;



    public GameObject Shoot(Transform bulletSource) {
        if(CanShoot()) {
            audioData.PlayOneShot(ShootSound, 0.1f);
            BulletsInCurrentMagazine--;
            ResetShootTimer();

            RaycastHit hit;
            Vector3 bulletDirection = bulletSource.forward;
            bulletDirection.x += Random.Range(-Spread, Spread);
            bulletDirection.y += Random.Range(-Spread, Spread);
            bulletDirection.z += Random.Range(-Spread, Spread);
            if(Physics.Raycast(bulletSource.position, bulletDirection, out hit)) {
                return hit.transform.gameObject;
            }
        }
        return null;
    }
    public bool Reload(Ammunition ammo, int ammunitionToReload) {
        if (!CanReload () || ammunitionToReload <= 0 || ammunitionToReload > MaxBulletsPerMagazine || IsReloading)
			return false;
        switch (Type) {
            case GunObject.GunType.Pistol:
                if (ammo.pistolAmmo <= 0) return false;
                break;
            case GunObject.GunType.MachineGun:
                if (ammo.machineGunAmmo <= 0) return false;
                break;
        }
        StartCoroutine (OnReload(ammo, ammunitionToReload));
		return true;
    }
    public void AimDownSight() {
        IsAimedDownSight = true;
        Spread = adsSpread;
    }
    public void HipFire() {
        IsAimedDownSight = false;
        Spread = hipFireSpread;
    }



    private void ResetShootTimer() { ShootTimer = 0; }
    private bool CanShoot() {
        return
            BulletsInCurrentMagazine > 0 &&
            ShootTimer > TimeBetweenShots &&
            !IsReloading;
    }
    private bool CanReload() {
        return
            BulletsInCurrentMagazine < MaxBulletsPerMagazine &&
            !IsReloading;
    }



    private void UpdateTimers() { ShootTimer += Time.deltaTime * 1000; }
    private void UpdateGunPosition() { transform.localPosition = IsAimedDownSight ? adsPosition : hipFirePosition; }

    IEnumerator OnReload(Ammunition ammo, int ammunitionToReload) {  
		audioData.PlayOneShot (ReloadSound, 0.1f);
		IsReloading = true;
		yield return new WaitForSeconds (TimeToReload/1000);
		IsReloading = false;
        int ammoReloaded = 0;
        switch (Type) {
            case GunObject.GunType.Pistol:
                ammoReloaded = ammo.GetAmmo(Ammunition.AmmoType.Pistol, ammunitionToReload);
                break;
            case GunObject.GunType.MachineGun:
                ammoReloaded = ammo.GetAmmo(Ammunition.AmmoType.MachineGun, ammunitionToReload);
                break;
        }
        BulletsInCurrentMagazine += ammoReloaded;
	}
}