﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraController : MonoBehaviour {
    private const float MAX_VERTICAL = 60;
    private const float MIN_VERTICAL = -60;


    /* Initializes all private variables */
    void Start() {
        VerticalLookSensitivity = 5;
        HorizontalLookSensitivity = 5;
        enabled = false; // Update function won't be called
    }



    public float VerticalLookSensitivity { get; set; }
    public float HorizontalLookSensitivity { get; set; }
    public float VerticalRotation { get; set; }
    public float HorizontalRotation { get; set; }


    /* Update function that gets called by the Player object */
    public void UpdateLook() {
        UpdateRotation();
    }
    private void UpdateRotation()
    {
        if (UserInterface.userInterface.IsPaused || UserInterface.userInterface.IsGameOver) return;
        HorizontalRotation = transform.parent.transform.localEulerAngles.y + Input.GetAxis("Mouse X") * HorizontalLookSensitivity;
        VerticalRotation += Input.GetAxis("Mouse Y") * VerticalLookSensitivity;
        VerticalRotation = Mathf.Clamp(VerticalRotation, MIN_VERTICAL, MAX_VERTICAL);

        transform.parent.transform.localEulerAngles = new Vector3(0, HorizontalRotation, 0);
        transform.localEulerAngles = new Vector3(-VerticalRotation, 0, 0);
    }
}
