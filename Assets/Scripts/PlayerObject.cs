﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObject : MoveableObject {
    public enum GunType { Primary, Secondary, Empty }

    private AudioSource audioData;

    private const float WALKING_SPEED = 6f;
    private const float RUNNING_SPEED = 10f;
    private const float JUMP_FORCE = 300f;
    private const float MAX_HEALTH = 100f;



    protected override void Start() {
        base.Start();
        MoveDirection = Vector3.zero;
        MaxHealth = Health = MAX_HEALTH;
        WalkSpeed = WALKING_SPEED;
        RunSpeed = RUNNING_SPEED;
        JumpForce = JUMP_FORCE;
        Walk();
        IsAlive = true;

        Player = this;
        audioData = GetComponent<AudioSource> ();
		Ammo = GetComponent<Ammunition> ();
        Camera = GameObject.Find("Camera");
        ActionController = new PlayerActionController();
        CameraController = GetComponentInChildren<PlayerCameraController>();
        PrimaryWeapon = GameObject.Find("MachineGun").GetComponent<GunObject>();
        SecondaryWeapon = GameObject.Find("Pistol").GetComponent<GunObject>();
		CurrentWeapon = PrimaryWeapon;
		UpdateCurrentWeapon ();
		Reload ();
    }
    protected override void Update() {
        base.Update();
        ActionController.UpdateAction();
        ActionController.UpdateDirection();
        CameraController.UpdateLook();
    }
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.name.Contains("Ammo")) {
            audioData.PlayOneShot(other.GetComponent<AmmoDrop>().pickupSound, 0.1f);
			switch (other.GetComponent<AmmoDrop> ().type) {
			case AmmoDrop.AmmoType.Pistol:
				Ammo.AddAmmo (Ammunition.AmmoType.Pistol, other.GetComponent<AmmoDrop> ().amount);
				break;
			case AmmoDrop.AmmoType.MachineGun:
				Ammo.AddAmmo (Ammunition.AmmoType.MachineGun, other.GetComponent<AmmoDrop> ().amount);
				break;
			}
            Destroy(other.gameObject);
        }
    }
    protected override void OnDeath() { UserInterface.userInterface.IsGameOver = true; }



    public static PlayerObject Player { get; private set; }
    public static GameObject Camera { get; private set; }

    private PlayerActionController ActionController { get; set; }
    private PlayerCameraController CameraController { get; set; }

	public Ammunition Ammo { get; set; }

    public GunObject PrimaryWeapon { get; set; }
    public GunObject SecondaryWeapon { get; set; }
    public GunObject CurrentWeapon { get; set; }

    public GunType CurrentGunType { get; set; }

    public bool Shoot() {
        GameObject shotGameObject = CurrentWeapon.Shoot(Camera.transform);
        if(shotGameObject != null && shotGameObject.name.Contains("Enemy")) {
            UserInterface.userInterface.HitmarkerTimer = 25;
            shotGameObject.GetComponent<MoveableObject>().Hurt(CurrentWeapon.DamagePerShot);
            return true;
        }
        return false;
    }

	public void Reload() {
        CurrentWeapon.Reload(Ammo, CurrentWeapon.MaxBulletsPerMagazine - CurrentWeapon.BulletsInCurrentMagazine);
	}


    private void UpdateCurrentWeapon() {
        switch(CurrentGunType) {
            case GunType.Primary:
                CurrentWeapon = PrimaryWeapon;
                PrimaryWeapon.gameObject.SetActive(true);
                SecondaryWeapon.gameObject.SetActive(false);
                SecondaryWeapon.IsReloading = false;
                break;
            case GunType.Secondary:
                CurrentWeapon = SecondaryWeapon;
                PrimaryWeapon.gameObject.SetActive(false);
                SecondaryWeapon.gameObject.SetActive(true);
                PrimaryWeapon.IsReloading = false;
                break;
            case GunType.Empty:
                CurrentWeapon = null;
                PrimaryWeapon.gameObject.SetActive(false);
                SecondaryWeapon.gameObject.SetActive(false);
                break;
        }
    }


    public class PlayerActionController {

        public void UpdateAction() {
            if (UserInterface.userInterface.IsPaused || UserInterface.userInterface.IsGameOver) return;
            // Left Mouse Click
            if (Player.CurrentWeapon.Mode == GunObject.FireMode.Single) {
                if(Input.GetMouseButtonDown(0)) {
                    if (Player.Shoot()) ;// GameButtonManager.Hit();
                }
            }
            else if(Player.CurrentWeapon.Mode == GunObject.FireMode.Auto) {
                if(Input.GetMouseButton(0)) {
                    if (Player.Shoot()) ;// GameButtonManager.Hit();
                }
            }

            // Right Mouse Click
            if(Input.GetMouseButtonDown(1)) {
                Player.CurrentWeapon.AimDownSight();
            }
            if(Input.GetMouseButtonUp(1)) {
                Player.CurrentWeapon.HipFire();
            }

            // Key Down
            if (Input.GetKeyDown(KeyCode.Space)) Player.Jump();
            if (Input.GetKeyDown(KeyCode.LeftShift)) Player.Run();
			if (Input.GetKeyDown (KeyCode.R)) Player.Reload ();
            if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetAxis("Mouse ScrollWheel") > 0) {
                Player.CurrentGunType = GunType.Primary;
                Player.UpdateCurrentWeapon();
            }
            if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetAxis("Mouse ScrollWheel") < 0) {
                Player.CurrentGunType = GunType.Secondary;
                Player.UpdateCurrentWeapon();
            }

            // Key Up
            if (Input.GetKeyUp(KeyCode.LeftShift)) Player.Walk();
        }

        public void UpdateDirection() {
            Player.MoveDirection = new Vector3(Input.GetAxis("Horizontal"), Player.MoveDirection.y, Input.GetAxis("Vertical"));
            Player.MoveDirection = Player.transform.TransformDirection(Player.MoveDirection);
            if (Player.IsGrounded) Player.MoveDirection.y = 0;
            Player.MoveDirection.Normalize();
        }
    }
}