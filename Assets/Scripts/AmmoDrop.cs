﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoDrop : MonoBehaviour {

    public enum AmmoType { Pistol, MachineGun }

	public float timeToDespawn;
    public AmmoType type;
    public AudioClip pickupSound;
    public int amount;

	void Start () {
		StartCoroutine ("OnDrop");
	}

	IEnumerator OnDrop() {
		yield return new WaitForSeconds(timeToDespawn);
		Destroy (gameObject);
	}
}
